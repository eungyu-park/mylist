#include <stdlib.h>
#include <stdio.h>
#include "mylist.h"

void mylist_init(struct mylist* l)
{
	// TODO: Fill it
	l->head = NULL;
}

void mylist_destroy(struct mylist* l)
{
	// TODO: Fill it
	struct mylist_node *curr = l->head;
	struct mylist_node *next;
	while (curr) {
		next = curr->next;
		//printf("leak test_%d cleaned\n", curr->data);
		free(curr);
		curr = next;
	}
}

void mylist_insert(
	struct mylist* l,
	struct mylist_node* before, int data)
{
	// TODO: Fill it
	struct mylist_node *newNode = (struct mylist_node *)malloc(sizeof(struct mylist_node));
	newNode->next = NULL;

	if (before == NULL) {
		newNode->data = data;
		newNode->next = l->head;
		l->head = newNode;
	}
	else {
		newNode->data = data;
		newNode->next = before->next;
		before->next = newNode;
	}
	//printf("leak test_%d allocated\n", newNode->data);
}

void mylist_remove(
	struct mylist* l,
	struct mylist_node* target)
{
	// TODO: Fill it
	struct mylist_node *curr = mylist_find(l, target->data);
	struct mylist_node *front = l->head;
	
	if (curr == mylist_get_head(l)) {
		l->head = curr->next;
		//printf("leak test_%d cleaned\n", curr->data);
		free(curr);
	}
	else {
		while (front->next)
		{
			if (front->next == curr) {
				front->next = curr->next;
				//printf("leak test_%d cleaned\n", curr->data);
				free(curr);
				break;
			}
			front = front->next;
		}
	}
}

struct mylist_node* mylist_find(struct mylist* l, int target)
{
	// TODO: Fill it
	struct mylist_node *curr = l->head;
	while (curr)
	{
		if (curr->data == target) {
			return curr;
		}
		else {
			curr = curr->next;
		}
	}
	return NULL;
}

struct mylist_node* mylist_get_head(struct mylist* l)
{
	// TODO: Fill it
	return l->head;
}

void mylist_print(const struct mylist* l)
{
	for (struct mylist_node* pointer = l->head;
		pointer != NULL;
		pointer = pointer->next) {
		printf("%d\n", pointer->data);
	}
}
